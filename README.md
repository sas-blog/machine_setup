# Machine Setup

These are instructions and scripts for setting LabVIEW machines for both Windows and Linux for use as both Development Machines and Runners. It is a work in progres...

## Creating Windows 10 VMs

I use KVM, so here are the KVM instructions. I start here: https://github.com/Fmstrat/winapps/blob/main/docs/KVM.md with the following modifications

when creating the VM
- I generally use 12288MB (12GB) for the RAM and 2 CPUs.
- I also have a doublewide screen so I have to edit the Video RAM in order to get the fullscreen to work.
``` xml
<video>
  <model type="qxl" ram="65536" vram="65536" vgamem="32768" heads="1" primary="yes"/>
  <alias name="video0"/>
  <address type="pci" domain="0x0000" bus="0x00" slot="0x01" function="0x0"/>
</video>
```

When running VM first time (during initial creation)
- make sure to install Win10 Pro
- Enable RDP
- Hostname I don't care.
- Do install spice guest tools
- DO NOT install registry entry it tells you to install.

After following all of the above, you should have a generic Win10 machine setup.

I will typically make sure I run all Windows updates so Win is up to date. Then I will use this as a starter disk image (sometimes after running the steps below - depending on my use case.) 

First I'll run the starter image to make sure Windows is up to date. Then I'll copy it with the name I want to give my new machine. Then I'll create a new machine from that disk, selecting VIRTIO for the disk and network drivers, and setting the clock settings and video settings in the XML.



### Initial Installations and Machine Setup


I usually install a few other things
- "Git for Windows"
- NIPM
- chocolately 

**TODO** - come up with a script for installing those.

Next for GitLab KVM executor it is nice to be able to SSH into the Windows box.

Install SSH server windows addon.
from host grab ssh key (create if necessary) - put in <programData>\ssh\administrator_authorized_keys. find powershell script online to set permissions correctly for this.

**TODO** - add script for this

